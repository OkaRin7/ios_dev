import UIKit

class CustomTableViewCell: UITableViewCell {


    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var date: UILabel!
    
    func configure (with object :Result){
        self.label.text = String(object.score!)
        self.date.text = object.date
   }
    

}
