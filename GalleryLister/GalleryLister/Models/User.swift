import Foundation

struct User: Codable {
    
    // MARK: - Public Properties
    
    let name: String
    
    // MARK: - Public Nested
    
    private enum CodingKeys: String, CodingKey {
        case name
    }
    
}

