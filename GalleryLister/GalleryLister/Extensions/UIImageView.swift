import UIKit

extension UIImageView {
    
    func reset() {
        image = UIImage(imageLiteralResourceName: "photoBack")
    }
    
}

