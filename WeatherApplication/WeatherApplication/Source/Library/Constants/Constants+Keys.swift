import Foundation

extension Constants {
  enum Keys {}
}

extension Constants.Keys {
  
  enum MapAnnotation {
    static let kMapAnnotationViewIdentifier = "de.erikmaximilianmartens.nearbyWeather.WeatherLocationMapAnnotationView"
  }
}

extension Constants.Keys {
    enum UserDefaults {
         static let kSavedNameArray = "SavedNameArray"
    }
}


