import Foundation

extension Constants {
  enum Identifier {}
}

extension Constants.Identifier {
    enum ReuseIdentifier {
        static let kLocationResultCell = "LocationResultCell"
    }
    
    enum ViewController {
        static let kWeatherDetailViewController = "WeatherDetailViewController"
        static let kForecastViewController = "ForcastViewController"
    }
    
    enum Segue {
        static let kShowLocationSearch = "showLocationSearch"
    }
}
