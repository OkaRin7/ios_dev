import Foundation

extension Constants {
    enum Urls {
       
        static let kOpenWeatherBaseUrl = URL(string: "https://api.openweathermap.org")!
    }
}
