import Foundation

extension Constants {
  enum Image {}
}

extension Constants.Image {
    enum name {
        static let kVerticalCloseButton = "VerticalCloseButton"
        static let kSadCloud = "SadCloud"
    }
}

extension Constants {
  enum html {}
}

extension Constants.html {
    enum text {
        static let htmlText = """
        <p>iOS Weather Application using OpenWeatherMap API.</p>
                <p>WeatherApplication is a nifty weather program, that provides current weather info for nearby towns, as well for bookmarked cities.</p>
"""
    }
}
