import Foundation

extension Constants {
  enum Values {}
}

extension Constants.Values {
  
  enum TemperatureName {
    static let kCelsius = "Celsius"
    static let kFahrenheit = "Fahrenheit"
    static let kKelvin = "Kelvin"
  }
}

extension Constants.Values {
  
  enum TemperatureUnit {
    static let kCelsius = "°C"
    static let kFahrenheit = "°F"
    static let kKelvin = "K"
  }
}
