import UIKit

class TabBarViewController: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        if let index = self.tabBar.items?.firstIndex(of: item), let subView = tabBar.subviews[index+1].subviews.first as? UIImageView {
            self.performSpringAnimation(imgView: subView)
        }
    }
    
    func performSpringAnimation(imgView: UIImageView) {
        imgView.animate(from: 1.1, to: 1)
    }
}
