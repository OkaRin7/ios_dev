import UIKit

class ForecastDayTableViewCell: UITableViewCell {
    
    // MARK: - Variables
    @IBOutlet var weatherConditionImageView: UIImageView!
    @IBOutlet var weekdayLabel: UILabel!
    @IBOutlet var weatherConditionLabel: UILabel!
    @IBOutlet var temperatureLabel: UILabel!
}
